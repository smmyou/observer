﻿namespace Observer
{
    internal interface IInvestor
    {
        void Update(Stock stock);
    }
}